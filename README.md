# DGTLS exercises

## news-tool
in the data/ folder you will find a news.json - file including classic news data.
your job is to create a news-web-module.
an html5-website using your weapons of choice.
the job (frontend and backend parts) should be solved in one (work) day.

### frontend part:
create a news-frontend-module with the following features:
- basic html5 template including all necessary head (meta?) and body tags
- a sortable and filterable news-list, generated from the news.json in the data-folder (live - meaning, when you edit the news.json, the changes will show on browser-refresh at least)
- autocompletion on the filter is definitely nice to have

### backend part
create a news-backend-module with the following features:
- persist your news in a database
- implement a news-api with the basic CRUD-operations for single elements and lists/json-arrays of news (include postman-file if used)
- a simple test is definitely nice to have

### additional duties
- write 5 lines about what you did and why you did it in this way (in english) - as txt/md file
- write a short (5 lines MAX) instruction on how to use your example - as txt/md file
- pack all your files - including your description and instruction - into an unprotected zip / tarball
- provide your zipfile/tarball via email to s.maier@dgtls.com or personally to your contact person at DGTLS 

### hints
- start with what you are most comfortable with.
- show us your skillset but also your capability of time- and self-management.
- (hopefully) have fun!


